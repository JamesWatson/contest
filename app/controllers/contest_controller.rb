class ContestController < ApplicationController
  def round
    if !session[:pair]
      @pair = Person.random_pair
      session[:pair] = @pair.map(&:id)
    else
      load_pair
    end
    @rounds = (session[:rounds] ||= person.rounds + 1)
  end

  def round_results
    load_pair

    case params[:choice]
    when 'left'
      @pair.first.beats(@pair.last)
    when 'right'
      @pair.last.beats(@pair.first)
    else
      redirect_to action: :round and return
    end

    session[:pair] = Person.random_pair.pluck(:id) if session[:pair]
    @person.rounds_save(session[:rounds] += 1)

    redirect_to action: :round
  end

  def scores
    @people = Person.where('imaged IS NOT NULL').order(score: :desc).limit(100)
    @max_score = @people.first.score
    @min_score = @people.last.score
  end

  private

  def load_pair
    @pair = person.load_pair(session[:pair])
  end
end
