class PeopleController < ApplicationController
  before_action :set_person, only: [:show, :edit, :update, :destroy]

  def profile
    @profile = Person.find(params[:id])
  end

  def show
  end

  def edit
  end

  def new
  end

  def create
    if Person.new(person_params).save
      redirect_to @person, notice: 'Person was successfully created.' 
    else
      render :new
    end
  end

  def update
    if @person.update(person_params)
      redirect_to @person, notice: 'Person was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @person.destroy
    redirect_to people_url, notice: 'Person was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_person
      @person = Person.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def person_params
      params.require(:person).permit(:image, :name, :university, :score, :rounds, :quotes)
    end
end
