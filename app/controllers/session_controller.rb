class SessionController < ApplicationController
  def welcome
    redirect_to home_path if lin?
    srand
    session[:state] ||= Digest::MD5.hexdigest(rand.to_s)
    @vk_url = VkontakteApi.authorization_url(scope: [:notify, :photos], state: session[:state])
  end

  def welcome_callback
    if session[:state].present? && session[:state] != params[:state]
      redirect_to root_url, alert: (I18n.t 'session.api_error') and return
    end

    @vk = VkontakteApi.authorize(code: params[:code])
    info = @vk.users.get(uid: @vk.user_id, fields: Person::VK_REQUEST_FIELDS).first
    @person = Person.vk(info)

    session[:token] = @vk.token
    session[:id] =  @person.id
    flash[:notice] = I18n.t 'session.welcome' + (@person.updated_at ? '_back' : '')

    redirect_to root_path
  end

  def logout
    session[:id], session[:token] = nil, nil
    flash[:notice] = I18n.t 'session.logout'
    redirect_to root_url
  end

  # <shit starts below this line>
  def dev_list
    @people = Person.all
  end

  def dev_login
    session[:id] = params[:id]
    redirect_to root_path
  end
end
