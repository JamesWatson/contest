class VkImageController < ApplicationController
  before_action :set_vk_client

  def albums
    @download_t = Benchmark.realtime do
      @albums = @vk.photos.get_albums(owner_id: person.uid, need_covers: 1, photo_sizes: 1)
      @photos = @vk.photos.get(owner_id: person.uid, album_id: :profile, rev: 1, photo_sizes: 1)
    end
  end

  def album
    redirect_to action: :albums and return if params[:album_id].nil?

    @album_title, @album_id = params[:album_title], params[:album_id].to_i
    @photos = @vk.photos.get(owner_id: person.uid, album_id: @album_id, rev: 1, photo_sizes: 1)
  end

  def show
    @photo = vk_photo(person.uid, params[:photo_id])
    @album_id, @album_title = params[:album_id], params[:album_title]
    redirect_to action: :albums if @photo.nil?
  end

  def select
    @photo = vk_photo(person.uid, params[:photo_id])
    redirect_to action: :albums and return if @photo.nil?
    person.set_image(@photo)
    if person.save
      redirect_to root_path
    else
      flash[:warning] = person.errors.messages.values.join(', ')
      redirect_to action: :albums
    end
  end

  def unselect
    person.update(image_hash: nil)
    redirect_to :back
  end

  private

  def vk_photo(uid, pid)
    @photo = @vk.photos.get_by_id(
      photos: "#{uid}_#{pid}",
      photo_sizes: 1
    ).first
  end

  def set_vk_client
    @vk = VkontakteApi::Client.new(session[:token])
  end
end
