module People::Round
  extend ActiveSupport::Concern

  def rounds_save(cnt)
    return unless cnt % 10 == 0
    self.update(rounds: cnt)
  end

  def load_pair(pair_ids)
    way = pair_ids.first > pair_ids.last ? 'ASC' : 'DESC'
    pair = Person.where(id: pair_ids).order('people.id ' + way).all.to_a
    raise 'Could not load pair' unless pair.size == 2
    pair
  end

  module ClassMethods
    def random_pair
      pair = Person.where('imaged IS NOT NULL').order('random()').limit(2)
      raise 'Need at least 2 contestants' unless pair.size == 2
      pair
    end
  end
end