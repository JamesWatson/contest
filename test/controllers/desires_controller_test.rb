require 'test_helper'

class DesiresControllerTest < ActionController::TestCase
  test "should get star" do
    get :star
    assert_response :success
  end

  test "should get unstar" do
    get :unstar
    assert_response :success
  end

end
