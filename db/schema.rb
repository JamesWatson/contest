# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150508205410) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "people", force: :cascade do |t|
    t.integer  "uid"
    t.string   "first_name",                                   null: false
    t.string   "last_name",                                    null: false
    t.integer  "sex",                                          null: false
    t.integer  "university"
    t.string   "university_name"
    t.string   "image_json",      limit: 2048
    t.float    "score",                        default: 400.0
    t.integer  "rounds",                       default: 0
    t.text     "activities"
    t.text     "interests"
    t.text     "music"
    t.text     "movies"
    t.text     "tv"
    t.text     "books"
    t.text     "games"
    t.text     "about"
    t.text     "quotes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_big"
    t.string   "imaged"
  end

  add_index "people", ["image_json"], name: "index_people_on_image_json", using: :btree
  add_index "people", ["score"], name: "index_people_on_score", using: :btree
  add_index "people", ["sex", "university"], name: "index_people_on_sex_and_university", using: :btree
  add_index "people", ["uid"], name: "index_people_on_uid", unique: true, using: :btree

  create_table "stars", force: :cascade do |t|
    t.integer  "liker_id"
    t.integer  "like_id"
    t.boolean  "mutual",     default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "stars", ["like_id"], name: "index_stars_on_like_id", using: :btree
  add_index "stars", ["liker_id", "like_id"], name: "index_stars_on_liker_id_and_like_id", unique: true, using: :btree
  add_index "stars", ["liker_id"], name: "index_stars_on_liker_id", using: :btree
  add_index "stars", ["mutual"], name: "index_stars_on_mutual", using: :btree

end
