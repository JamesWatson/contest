class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.integer :uid
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.integer :sex, null: false
      t.integer :university
      t.string :university_name

      t.string :image_hash, limit: 2048
      t.float :score, default: 400
      t.integer :rounds, default: 0

      t.text :activities
      t.text :interests
      t.text :music
      t.text :movies
      t.text :tv
      t.text :books
      t.text :games
      t.text :about
      t.text :quotes

      t.index :image_hash
      t.index :uid, unique: true
      t.index :score
      t.index [:sex, :university]

      t.timestamps
    end
  end
end
