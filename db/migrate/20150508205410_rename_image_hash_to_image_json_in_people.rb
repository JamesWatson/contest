class RenameImageHashToImageJsonInPeople < ActiveRecord::Migration
  def change
    rename_column :people, :image_hash, :image_json
  end
end
